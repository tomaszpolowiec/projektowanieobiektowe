import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by Tomek93 on 2017-12-18.
 */


class CsvRoutingParser
{
    HashMap<String, String> getRoutes = new HashMap<String, String>();
    HashMap<String, String> postRoutes = new HashMap<String, String>();
    HashMap<String, String> putRoutes = new HashMap<String, String>();
    HashMap<String, String> deleteRoutes = new HashMap<String, String>();

    void parse(String fileName)
    {
        try
        {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNext()){
                String line = scanner.nextLine();
                String[] arr = line.split(",");
                if(arr[0].equals("GET"))
                {
                    getRoutes.put(arr[1], arr[2]);
                }
                else if(arr[0].equals("POST"))
                {
                    postRoutes.put(arr[1], arr[2]);
                }
                else if(arr[0].equals("PUT"))
                {
                    putRoutes.put(arr[1], arr[2]);
                }
                else if(arr[0].equals("DELETE"))
                {
                    deleteRoutes.put(arr[1], arr[2]);
                }
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class Routing {
    static CsvRoutingParser crp = new CsvRoutingParser();

    public static void main(String[] args) throws Exception {

        crp.parse("routing.csv");

        HttpServer server = HttpServer.create(new InetSocketAddress(9000), 0);
        server.createContext("/", new MainHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }
    static class MainHandler implements HttpHandler {

        public void handle(HttpExchange t) throws IOException {
            this.execute(t);
        }

        public void execute(HttpExchange t)
        {
            if( t.getRequestMethod().equals("GET") )
            {
                String patch = t.getRequestURI().getPath().substring(1);
                if( crp.getRoutes.containsKey(patch)) {
                    String routeVal = crp.getRoutes.get(patch);
                    try {
                        TemplateBuilder tmpl = new HTMLTemplateBuilder();
                        tmpl.pobierzZmienneJson(routeVal+".json");
                        tmpl.pobierzTemplate(routeVal+".html");
                        tmpl.podstawZmienne();
                        HTMLTemplate html = tmpl.buduj();
                        Iterator<String> it = html.lines.iterator();
                        String response = "";
                        while(it.hasNext())
                        {
                            response += it.next();
                        }

                        Headers headers = t.getResponseHeaders();
                        headers.add("Content-Type", "text/html");
                        t.sendResponseHeaders(200, response.length());
                        OutputStream os = t.getResponseBody();
                        os.write(response.getBytes());
                        os.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                else
                {
                    this.delegate(t);
                }
            }
            else
            {
              this.delegate(t);
            }
        }

        public void delegate(HttpExchange t)
        {
            JSONRequestHandler jrh = new JSONRequestHandler();
            jrh.execute(t);
        }
    }

    static class JSONRequestHandler
    {
        private String getStringResponse(HttpExchange t, String routeVal)
        {
            String response = "{ \"method\": \""+t.getRequestMethod()+"\", \"operation\":\""+routeVal+"\", \"status\": \"Ok\" }";
            return response;
        }

        private void doResponse(HttpExchange t, String routeVal) throws IOException {
            Headers headers = t.getResponseHeaders();
            headers.add("Content-Type", "application/json");
            String response = this.getStringResponse(t, routeVal);
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        public void execute(HttpExchange t)
        {
            try {
                if (t.getRequestMethod().equals("POST")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.postRoutes.containsKey(patch)) {
                        String routeVal = crp.postRoutes.get(patch);
                        this.doResponse(t, routeVal);
                    } else {
                        this.delegate(t);
                    }
                } else if (t.getRequestMethod().equals("PUT")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.putRoutes.containsKey(patch)) {
                        String routeVal = crp.putRoutes.get(patch);
                        this.doResponse(t, routeVal);
                    } else {
                        this.delegate(t);
                    }
                } else if (t.getRequestMethod().equals("DELETE")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.deleteRoutes.containsKey(patch)) {
                        String routeVal = crp.deleteRoutes.get(patch);
                        this.doResponse(t, routeVal);
                    } else {
                        this.delegate(t);
                    }
                } else {
                    this.delegate(t);
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }

        public void delegate(HttpExchange t)
        {
            ErrorHandler eh = new ErrorHandler();
            eh.execute(t);
        }
    }

    static class ErrorHandler
    {
        void execute(HttpExchange t)
        {
            try {
                int respCode;
                TemplateBuilder tmpl = new HTMLTemplateBuilder();
                if (t.getRequestMethod().equals("GET"))
                {
                    tmpl.pobierzZmienneJson("errorGet404.json");
                    respCode = 404;
                }
                else if(t.getRequestMethod().equals("POST") || t.getRequestMethod().equals("PUT") || t.getRequestMethod().equals("DELETE"))
                {
                    tmpl.pobierzZmienneJson("errorJson.json");
                    respCode = 404;
                }
                else
                {
                    tmpl.pobierzZmienneJson("errorMethod.json");
                    respCode = 500;
                }
                tmpl.pobierzTemplate("errorTemplate.html");
                tmpl.podstawZmienne();
                HTMLTemplate html = tmpl.buduj();
                Iterator<String> it = html.lines.iterator();
                String response = "";
                while(it.hasNext())
                {
                    response += it.next();
                }
                Headers headers = t.getResponseHeaders();
                headers.add("Content-Type", "text/html");
                t.sendResponseHeaders(respCode, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
