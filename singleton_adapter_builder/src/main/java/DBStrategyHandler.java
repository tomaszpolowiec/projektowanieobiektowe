import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Tomek93 on 2018-01-10.
 */
interface DBStrategyInterface
{
    ArrayList<Student> getAllStudents() throws SQLException;
    Student getStudentById(int id) throws  SQLException;
    Student insertStudent(Student s) throws SQLException;
    Student updateStudent(Student s) throws SQLException;
    boolean deleteStudent(Student s) throws SQLException;
}

public class DBStrategyHandler implements DBStrategyInterface
{
    private DBStrategyInterface strategy = null;

    DBStrategyHandler(DBStrategyInterface strategy)
    {
        this.strategy = strategy;
    }

    void setStrategy(DBStrategyInterface strategy)
    {
        this.strategy = strategy;
    }

    public ArrayList<Student> getAllStudents() throws SQLException {
        return this.strategy.getAllStudents();
    }

    public Student getStudentById(int id) throws SQLException {
        return this.strategy.getStudentById(id);
    }

    public Student insertStudent(Student s) throws SQLException {
        return this.strategy.insertStudent(s);
    }

    public Student updateStudent(Student s) throws SQLException {
        return this.strategy.updateStudent(s);
    }

    public boolean deleteStudent(Student s) throws SQLException {
        return this.strategy.deleteStudent(s);
    }

    public static void main(String[] args)
    {
        try {
            //DBStrategyHandler db = new DBStrategyHandler(new DBMysqlStrategy());
            DBStrategyHandler db = new DBStrategyHandler(new DBSqliteStrategy());
            ArrayList<Student> students = db.getAllStudents();
            System.out.println(students.size());
            Student s = new Student();
            s.imie = "Grzegorz";
            s.nazwisko = "Brzeczyszczykiewicz";
            s = db.insertStudent(s);
            s.imie = "Stefan";
            s = db.updateStudent(s);
            System.out.println(s);
            System.out.println("byId " + db.getStudentById(2345678));
            System.out.println("byId " + db.getStudentById(s.id));

            students = db.getAllStudents();
            Iterator<Student> it = students.iterator();
            while (it.hasNext()) {
                Student stud = it.next();
                System.out.println(stud.id + " " + stud.imie + " " + stud.nazwisko);
            }
            System.out.println(db.deleteStudent(s));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}

class DBSqliteStrategy implements DBStrategyInterface
{
    Adapter sqliteHandler = null;

    DBSqliteStrategy()
    {
        JsonConfigReader jsonReader = new JsonConfigReader();
        DbConfig conf = jsonReader.getConfigFromFile("sqliteDbConfig.json");
        String adapterString = conf.driver+":"+conf.url;
        sqliteHandler = new Adapter(adapterString);
    }

    public ArrayList<Student> getAllStudents() throws SQLException {
        return sqliteHandler.getAllStudents();
    }

    public Student getStudentById(int id) throws SQLException {
        return sqliteHandler.getStudentById(id);
    }

    public Student insertStudent(Student s) throws SQLException {
        s.nazwisko = s.nazwisko.toUpperCase();
        return sqliteHandler.insertStudent(s);
    }

    public Student updateStudent(Student s) throws SQLException {
        return sqliteHandler.updateStudent(s);
    }

    public boolean deleteStudent(Student s) throws SQLException {
        return sqliteHandler.deleteStudent(s);
    }
}

class DBMysqlStrategy implements DBStrategyInterface
{
    Adapter mysqlHandler = null;

    DBMysqlStrategy()
    {
        JsonConfigReader jsonReader = new JsonConfigReader();
        DbConfig conf = jsonReader.getConfigFromFile("sqliteDbConfig.json");
        String adapterString = conf.driver+"://"+conf.url;
        if(conf.port != null)
            adapterString += ":"+conf.port;
        adapterString +="/"+conf.database+"?user="+conf.user+"&password="+conf.passweord;
        mysqlHandler = new Adapter(adapterString);
    }

    public ArrayList<Student> getAllStudents() throws SQLException {
        return mysqlHandler.getAllStudents();
    }

    public Student getStudentById(int id) throws SQLException {
        return mysqlHandler.getStudentById(id);
    }

    public Student insertStudent(Student s) throws SQLException {
        return mysqlHandler.insertStudent(s);
    }

    public Student updateStudent(Student s) throws SQLException {
        return mysqlHandler.updateStudent(s);
    }

    public boolean deleteStudent(Student s) throws SQLException {
        return mysqlHandler.deleteStudent(s);
    }
}