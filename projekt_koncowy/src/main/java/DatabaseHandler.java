import java.sql.*;
import java.util.Iterator;
import java.util.Vector;

import org.sqlite.JDBC;

/**
 * Created by Tomek93 on 2017-10-25.
 */

class Student
{
    int id;
    String imie;
    String nazwisko;
    String toJsonString()
    {
        return "{\"id\": "+id+", \"imie\": \""+imie+"\", \"nazwisko\": \""+nazwisko+"\"}";
    }
};


public class DatabaseHandler {

    private static DatabaseHandler dbHandler = new DatabaseHandler();

    public static DatabaseHandler getInstance()
    {
        return DatabaseHandler.dbHandler;
    }

    Connection connection = null;

    DatabaseHandler()
    {
        try {
            this.connection = DriverManager.getConnection("jdbc:sqlite:identifier.sqlite");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Vector<Student> getAllStudents() throws SQLException
    {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);

        Vector<Student> res = new Vector<Student>();
        ResultSet wyniki = statement.executeQuery( "SELECT * FROM students");
        while( wyniki.next())
        {
            Student s = new Student();
            s.id = wyniki.getInt("id");
            s.imie = wyniki.getString("imie");
            s.nazwisko = wyniki.getString("nazwisko");
            res.add(s);
        }
        return res;
    }

    public Student getStudentById(int id) throws  SQLException
    {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);

        ResultSet wyniki = statement.executeQuery( "SELECT * FROM students WHERE id = "+id);
        while( wyniki.next())
        {
            Student s = new Student();
            s.id = wyniki.getInt("id");
            s.imie = wyniki.getString("imie");
            s.nazwisko = wyniki.getString("nazwisko");
            return s;
        }
        return null;
    }

    public Student insertStudent(Student s) throws SQLException {
        String query = " insert into students (imie, nazwisko)"
                + " values (?, ?)";
        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        preparedStmt.setString (1, s.imie);
        preparedStmt.setString (2, s.nazwisko);
        // execute the preparedstatement
        preparedStmt.execute();
        ResultSet rs = preparedStmt.getGeneratedKeys();
        if(rs.next())
        {
            int last_inserted_id = rs.getInt(1);
            s.id = last_inserted_id;
            return s;
        }
        return null;
    }

    public Student updateStudent(Student s) throws SQLException {
        String query = " UPDATE students SET imie = ? , nazwisko = ?  WHERE id = ? ";
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        preparedStmt.setString (1, s.imie);
        preparedStmt.setString (2, s.nazwisko);
        preparedStmt.setInt (3, s.id);
        // execute the preparedstatement
        preparedStmt.executeUpdate();
        int num = preparedStmt.getUpdateCount();
        if(num > 0)
        {
            return s;
        }
        return null;
    }

    public boolean deleteStudent(Student s) throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);

        int num = statement.executeUpdate( "DELETE FROM students WHERE id="+s.id );
        return num > 0;
    }

    public static void main(String args[])
    {
        try
        {
            DatabaseHandler db = DatabaseHandler.getInstance();
            Vector<Student> students = db.getAllStudents();
            System.out.println(students.size());
            Student s = new Student();
            s.imie= "Grzegorz"; s.nazwisko="Brzeczyszczykiewicz";
            s= db.insertStudent(s);
            s.imie = "Stefan";
            s= db.updateStudent(s);
            System.out.println(s);
            System.out.println(db.getStudentById(2345678));
            System.out.println(db.getStudentById(s.id));

            students = db.getAllStudents();
            Iterator<Student> it = students.iterator();
            while(it.hasNext())
            {
                Student stud = it.next();
                System.out.println(stud.id+" "+stud.imie+" "+stud.nazwisko);
            }
            System.out.println(db.deleteStudent(s));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
