import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Tomek93 on 2017-11-11.
 */

interface DbInterface
{
    ArrayList<Student> getAllStudents() throws  SQLException;
    Student getStudentById(int id) throws  SQLException;
    Student insertStudent(Student s) throws SQLException;
    Student updateStudent(Student s) throws SQLException;
    boolean deleteStudent(Student s) throws SQLException;
}

abstract class DbHandler implements DbInterface
{
    Connection connection = null;

    DbHandler()
    {
    }

    public ArrayList<Student> getAllStudents() throws SQLException
    {
        Statement statement = null;
        ResultSet wyniki = null;
        ArrayList<Student> res = new ArrayList<Student>();
        try
        {
            statement =  connection.createStatement();
            statement.setQueryTimeout(30);
            wyniki = statement.executeQuery( "SELECT * FROM students");
            while( wyniki.next())
            {
                Student s = new Student();
                s.id = wyniki.getInt("id");
                s.imie = wyniki.getString("imie");
                s.nazwisko = wyniki.getString("nazwisko");
                res.add(s);
            }
            statement.close();
        }
        catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if(wyniki != null)
                wyniki.close();
            if(statement != null)
                statement.close();
        }
        return res;
    }

    public Student getStudentById(int id) throws  SQLException
    {
        PreparedStatement statement = null;
        ResultSet wyniki = null;
        Student s = null;
        try {
            String query = "SELECT * FROM students WHERE id = ?";
            statement = connection.prepareStatement(query);
            statement.setQueryTimeout(30);
            statement.setInt(1, id);
            wyniki = statement.executeQuery();
            while (wyniki.next()) {
                s = new Student();
                s.id = wyniki.getInt("id");
                s.imie = wyniki.getString("imie");
                s.nazwisko = wyniki.getString("nazwisko");
            }
            wyniki.close();
            statement.close();
            return s;
        }
        finally
        {
            if(statement != null)
                statement.close();
            if(wyniki != null)
                wyniki.close();
        }
    }

    public Student insertStudent(Student s) throws SQLException
    {

        PreparedStatement preparedStmt = null;
        ResultSet rs = null;
        try
        {
            String query = " insert into students (imie, nazwisko)"
                    + " values (?, ?)";
            // create the mysql insert preparedstatement
            preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, s.imie);
            preparedStmt.setString(2, s.nazwisko);
            // execute the preparedstatement
            preparedStmt.execute();
            rs = preparedStmt.getGeneratedKeys();
            if (rs.next()) {
                int last_inserted_id = rs.getInt(1);
                s.id = last_inserted_id;
                preparedStmt.close();
                return s;
            }
            return null;
        }
        catch(SQLException ex)
        {
            ex.printStackTrace();
            return null;
        }
        finally
        {
            if(rs != null)
                rs.close();
            if(preparedStmt != null)
                preparedStmt.close();
        }
    }

    public Student updateStudent(Student s) throws SQLException
    {
        PreparedStatement preparedStmt = null;
        int num = 0;
        try {
            String query = " UPDATE students SET imie = ? , nazwisko = ?  WHERE id = ? ";
            preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, s.imie);
            preparedStmt.setString(2, s.nazwisko);
            preparedStmt.setInt(3, s.id);
            // execute the preparedstatement
            preparedStmt.executeUpdate();
            num = preparedStmt.getUpdateCount();
            preparedStmt.close();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            return null;
        }
        finally {
            if( preparedStmt != null )
                preparedStmt.close();
        }
        if(num > 0)
        {
            return s;
        }
        return null;
    }

    public boolean deleteStudent(Student s) throws SQLException {
        PreparedStatement statement = null;
        try
        {
            String query = "DELETE FROM students WHERE id= ?";
            statement = connection.prepareStatement(query);
            statement.setInt(1, s.id);
            statement.setQueryTimeout(30);
            int num = statement.executeUpdate();
            return num > 0;
        }
        catch (SQLException ex)
        {
            return false;
        }
        finally {
            if(statement != null)
                statement.close();
        }
    }
}

public class Adapter extends DbHandler implements DbInterface {

    static Adapter adapter= null;
    static
    {
        JsonConfigReader jsonReader = new JsonConfigReader();
        //DbConfig conf = jsonReader.getConfigFromFile("mysqlDbConfig.json");
        DbConfig conf = jsonReader.getConfigFromFile("sqliteDbConfig.json");
        if(conf.driver.equals("jdbc:mysql") || conf.driver.equals("jdbc:postgresql") )
        {
            String adapterString = conf.driver+"://"+conf.url;
            if(conf.port != null)
                adapterString += ":"+conf.port;
            adapterString +="/"+conf.database+"?user="+conf.user+"&password="+conf.passweord;
            adapter = new Adapter(adapterString);
        }
        else if(conf.driver.equals("jdbc:sqlite"))
        {
            String adapterString = conf.driver+":"+conf.url;
            adapter = new Adapter(adapterString);
        }
        else
        {
            adapter = null;
        }
    }

    private Adapter(){}
    /*private */Adapter(String driver)
    {
        try {
            this.connection = DriverManager.getConnection(driver);
        } catch (SQLException e) {
            //e.printStackTrace();
        }
    }

    static DbInterface getDbHandler(){ return adapter;}

    public static void main(String args[])
    {
        try
        {
            DbInterface db = Adapter.getDbHandler();
            ArrayList<Student> students = db.getAllStudents();
            System.out.println(students.size());
            Student s = new Student();
            s.imie= "Grzegorz"; s.nazwisko="Brzeczyszczykiewicz";
            s= db.insertStudent(s);
            s.imie = "Stefan";
            s= db.updateStudent(s);
            System.out.println(s);
            System.out.println("byId "+db.getStudentById(2345678));
            System.out.println("byId "+db.getStudentById(s.id));

            students = db.getAllStudents();
            Iterator<Student> it = students.iterator();
            while(it.hasNext())
            {
                Student stud = it.next();
                System.out.println(stud.id+" "+stud.imie+" "+stud.nazwisko);
            }
            System.out.println(db.deleteStudent(s));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
