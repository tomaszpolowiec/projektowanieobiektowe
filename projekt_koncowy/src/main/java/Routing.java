import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.*;

/**
 * Created by Tomek93 on 2017-12-18.
 */


class CsvRoutingParser
{
    HashMap<String, String> getRoutes = new HashMap<String, String>();
    HashMap<String, String> postRoutes = new HashMap<String, String>();
    HashMap<String, String> putRoutes = new HashMap<String, String>();
    HashMap<String, String> deleteRoutes = new HashMap<String, String>();

    void parse(String fileName)
    {
        try
        {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNext()){
                String line = scanner.nextLine();
                String[] arr = line.split(",");
                if(arr[0].equals("GET"))
                    getRoutes.put(arr[1], arr[2]);
                else if(arr[0].equals("POST"))
                    postRoutes.put(arr[1], arr[2]);
                else if(arr[0].equals("PUT"))
                    putRoutes.put(arr[1], arr[2]);
                else if(arr[0].equals("DELETE"))
                    deleteRoutes.put(arr[1], arr[2]);
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class Routing {
    static CsvRoutingParser crp = new CsvRoutingParser();

    public static void main(String[] args) throws Exception {

        crp.parse("routing.csv");

        HttpServer server = HttpServer.create(new InetSocketAddress(9000), 0);
        server.createContext("/", new MainHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }
    static class MainHandler implements HttpHandler {

        public void handle(HttpExchange t) throws IOException {
            this.execute(t);
        }

        public void execute(HttpExchange t)
        {
            if( t.getRequestMethod().equals("GET") )
            {
                String patch = t.getRequestURI().getPath().substring(1);
                if( crp.getRoutes.containsKey(patch)) {
                    String routeVal = crp.getRoutes.get(patch);
                    if(routeVal.equals("html")) {
                        try {
                            TemplateBuilder tmpl = new HTMLTemplateBuilder();
                            tmpl.pobierzZmienneJson("htmls/" + patch + ".json");
                            tmpl.pobierzTemplate("htmls/" + patch + ".html");
                            tmpl.podstawZmienne();
                            HTMLTemplate html = tmpl.buduj();
                            Iterator<String> it = html.lines.iterator();
                            String response = "";
                            while (it.hasNext()) {
                                response += it.next();
                            }

                            Headers headers = t.getResponseHeaders();
                            headers.add("Content-Type", "text/html");
                            t.sendResponseHeaders(200, response.length());
                            OutputStream os = t.getResponseBody();
                            os.write(response.getBytes());
                            os.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    else
                    {
                        this.delegate(t);
                    }
                }
                else
                {
                    this.delegate(t);
                }
            }
            else
            {
              this.delegate(t);
            }
        }

        public void delegate(HttpExchange t)
        {
            JSONRequestHandler jrh = new JSONRequestHandler();
            jrh.execute(t);
        }
    }

    static class JSONRequestHandler
    {
        Student getStudentFromRequestody(HttpExchange t)
        {
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new InputStreamReader(t.getRequestBody()));
                JSONObject jsonObject = (JSONObject) obj;
                Student s = new Student();
                if(jsonObject.containsKey("imie"))
                    s.imie = (String) jsonObject.get("imie");
                if(jsonObject.containsKey("nazwisko"))
                    s.nazwisko = (String) jsonObject.get("nazwisko");
                if(jsonObject.containsKey("id"))
                    s.id = ((Long) jsonObject.get("id")).intValue() ;
                return s;
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            return null;
        }

        private String getStringResponse(HttpExchange t)
        {
            String patch = t.getRequestURI().getPath().substring(1);
            if(patch.equals("student"))
                return studentJsonHandler(t);
            if(patch.equals("zegar")) {
                Date d = new Date();
                return "{\"time\": \""+d.toString()+ "\"}";
            }
            if(patch.equals("ip")) {
                return "{\"clientIp\": \""+t.getRemoteAddress().getHostName()+ "\", \"clientPort\": " + t.getRemoteAddress().getPort() + "}";
            }
            return "";
        }

        private String studentJsonHandler(HttpExchange t)
        {
            DbInterface db = Adapter.getDbHandler();
            if(t.getRequestMethod().equals("GET"))
            {
                try
                {
                    ArrayList<Student> student = db.getAllStudents();
                    Iterator<Student> it = student.iterator();
                    String response = "[";
                    int n = 0;
                    while(it.hasNext())
                    {
                        if(n> 0)
                            response += ", ";
                        response += it.next().toJsonString();
                        n++;
                    }
                    response+= "]";
                    return response;
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                }
                return "";
            }
            else
            {
                Student s = getStudentFromRequestody(t);
                Student resS = null;
                if(s == null)
                    return "{\"error\": \"Złe zapytanie\"}";
                try {
                    if (t.getRequestMethod().equals("POST")) {
                        resS = db.insertStudent(s);
                    } else if (t.getRequestMethod().equals("PUT")) {
                        resS = db.updateStudent(s);
                    } else if (t.getRequestMethod().equals("DELETE")) {
                        if (db.deleteStudent(s))
                            return "{\"status\": \"Usunięto studenta\"}";
                        else
                            return "{\"error\": \"Nie udało się usunąć\"}";
                    }
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                }
                if(resS == null)
                    return "{\"error\": \"Złe zapytanie\"}";
                else
                    return resS.toJsonString();
            }
        }

        private void doResponse(HttpExchange t, String routeVal) throws IOException {
            Headers headers = t.getResponseHeaders();
            headers.add("Content-Type", "application/json");
            String response = this.getStringResponse(t);
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        public void execute(HttpExchange t)
        {
            try {
                if (t.getRequestMethod().equals("GET")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.postRoutes.containsKey(patch)) {
                        String routeVal = crp.getRoutes.get(patch);
                        if(routeVal.equals("json"))
                            this.doResponse(t, patch);
                        else
                            this.delegate(t);
                    } else {
                        this.delegate(t);
                    }
                } else if (t.getRequestMethod().equals("POST")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.postRoutes.containsKey(patch)) {
                        String routeVal = crp.postRoutes.get(patch);
                        if(routeVal.equals("json"))
                            this.doResponse(t, patch);
                        else
                            this.delegate(t);
                    } else {
                        this.delegate(t);
                    }
                } else if (t.getRequestMethod().equals("PUT")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.putRoutes.containsKey(patch)) {
                        String routeVal = crp.putRoutes.get(patch);
                        if(routeVal.equals("json"))
                            this.doResponse(t, patch);
                        else
                            this.delegate(t);
                    } else {
                        this.delegate(t);
                    }
                } else if (t.getRequestMethod().equals("DELETE")) {
                    String patch = t.getRequestURI().getPath().substring(1);
                    if (crp.deleteRoutes.containsKey(patch)) {
                        String routeVal = crp.deleteRoutes.get(patch);
                        if(routeVal.equals("json"))
                            this.doResponse(t, patch);
                        else
                            this.delegate(t);
                    } else {
                        this.delegate(t);
                    }
                } else {
                    this.delegate(t);
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }

        public void delegate(HttpExchange t)
        {
            ErrorHandler eh = new ErrorHandler();
            eh.execute(t);
        }
    }

    static class ErrorHandler
    {
        void execute(HttpExchange t)
        {
            try {
                int respCode;
                TemplateBuilder tmpl = new HTMLTemplateBuilder();
                if (t.getRequestMethod().equals("GET"))
                {
                    tmpl.pobierzZmienneJson("htmls/errorGet404.json");
                    respCode = 404;
                }
                else if(t.getRequestMethod().equals("POST") || t.getRequestMethod().equals("PUT") || t.getRequestMethod().equals("DELETE"))
                {
                    tmpl.pobierzZmienneJson("htmls/errorJson.json");
                    respCode = 404;
                }
                else
                {
                    tmpl.pobierzZmienneJson("htmls/errorMethod.json");
                    respCode = 500;
                }
                tmpl.pobierzTemplate("htmls/errorTemplate.html");
                tmpl.podstawZmienne();
                HTMLTemplate html = tmpl.buduj();
                Iterator<String> it = html.lines.iterator();
                String response = "";
                while(it.hasNext())
                {
                    response += it.next();
                }
                Headers headers = t.getResponseHeaders();
                headers.add("Content-Type", "text/html");
                t.sendResponseHeaders(respCode, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
