import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

/**
 * Created by Tomek93 on 2017-12-17.
 */

class DbConfig
{
    String driver;
    String url;
    String user;
    String passweord;
    String port;
    String database;
}

public class JsonConfigReader
{
    JsonReader jr = new JsonReader();

    DbConfig getConfigFromFile(String filename)
    {
        File f = new File(filename);
        if(f.exists() && f.isFile())
            return jr.getConfigFromFile(filename);
        else
            return null;
    }

}

class JsonReader
{
    DbConfig getConfigFromFile(String filename)
    {
        DbConfig conf = new DbConfig();
        try
        {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(filename));

            JSONObject jsonObject = (JSONObject) obj;
            conf.driver = (String) jsonObject.get("driver");
            conf.url = (String) jsonObject.get("url");
            conf.user = (String) jsonObject.get("user");
            conf.passweord = (String) jsonObject.get("password");
            conf.database = (String) jsonObject.get("database");
            conf.port = (String) jsonObject.get("port");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return conf;
    }
}
