import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tomek93 on 2017-12-06.
 */

class HTMLTemplate
{
    Vector<String> lines = null;

    void returnHTML(String fileName)
    {
        try
        {
            PrintWriter pw = new PrintWriter(fileName);
            Iterator<String> it = this.lines.iterator();
            while(it.hasNext())
            {
                pw.println(it.next());
            }
            pw.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

}

interface TemplateBuilder
{
    void pobierzZmienneJson(String jsonFile);
    void pobierzTemplate(String templateFile);
    void podstawZmienne();
    HTMLTemplate buduj();
}

class TemplateVariable
{
    int nrLinii;
    String nazwaZmiennejl;
}

public class HTMLTemplateBuilder implements TemplateBuilder
{
    Map<String, String> wartosciZmiennych = new HashMap<String, String>();
    Vector<String> htmlLines = new Vector<String>();
    Vector<TemplateVariable> htmlZmienne = new Vector<TemplateVariable>();

    public void pobierzZmienneJson(String jsonFile)
    {
        try
        {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(jsonFile));

            JSONObject jsonObject = (JSONObject) obj;
            //System.out.println(jsonObject);
            for ( Object keys : jsonObject.keySet()) {
                //System.out.print( "  key = " + keys );
                String val = ( String ) jsonObject.get(keys);
                //System.out.println(" val= "+val);
                this.wartosciZmiennych.put((String)keys, val);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        //System.out.println(wartosciZmiennych);
    }

    public void pobierzTemplate(String templateFile)
    {
        Pattern MY_PATTERN = Pattern.compile("\\{\\{(.*?)\\}\\}");
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(templateFile));
            String line;
            int numerLini = 1;
            while ((line = reader.readLine()) != null) {
                htmlLines.add(line);
                Matcher m = MY_PATTERN.matcher(line);
                while (m.find()) {
                    String s = m.group(1);
                    //System.out.println(numerLini+": "+s);
                    TemplateVariable tmpVar = new TemplateVariable();
                    tmpVar.nrLinii = numerLini;
                    tmpVar.nazwaZmiennejl = s;
                    htmlZmienne.add(tmpVar);
                }
                numerLini++;
            }
            reader.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void podstawZmienne() {
        Iterator<TemplateVariable> it = htmlZmienne.iterator();
        while( it.hasNext() )
        {
            TemplateVariable tv = it.next();
            if(wartosciZmiennych.containsKey(tv.nazwaZmiennejl))
            {
                String val = wartosciZmiennych.get(tv.nazwaZmiennejl);
                String l = this.htmlLines.get(tv.nrLinii-1);
                l = l.replace("{{"+tv.nazwaZmiennejl+"}}"  ,val);
                htmlLines.set(tv.nrLinii-1, l);
            }
            else
            {
                System.err.print(tv.nrLinii+": Brak zmiennej -> "+tv.nazwaZmiennejl);
            }
        }
    }

    public HTMLTemplate buduj() {
        HTMLTemplate htmlTemp = new HTMLTemplate();
        htmlTemp.lines = htmlLines;
        return htmlTemp;
    }

    public static void main(String[] args)
    {
        TemplateBuilder tmpl = new HTMLTemplateBuilder();
        tmpl.pobierzZmienneJson("templateVariables.json");
        tmpl.pobierzTemplate("temp1.html");
        tmpl.podstawZmienne();
        HTMLTemplate html = tmpl.buduj();
        Iterator<String> it = html.lines.iterator();
        while(it.hasNext())
        {
            System.out.println(it.next());
        }
        html.returnHTML("html1.html");
    }
}
